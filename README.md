# Black night AMOLED theme, wide.

<div align='center'>
	<a href='https://gitlab.com/vitaly-zdanevich-styles/stackoverflow/-/raw/main/stackoverflow.user.css?ref_type=heads' alt='Install with Stylus'>
		<img src='https://img.shields.io/badge/Install%20directly%20with-Stylus-116b59.svg?longCache=true&style=flat' />
	</a>
</div>

Also you can install theme from https://userstyles.world/style/16068/stackoverflow-com-black-and-wide

![screenshot](/screenshot.png)

For use with [Stylus](https://github.com/openstyles/stylus) for [Chrome](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne) or [Firefox](https://addons.mozilla.org/firefox/addon/styl-us).
